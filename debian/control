Source: tree-style-tab
Section: web
Priority: optional
Standards-Version: 4.2.1
Maintainer: Debian Mozilla Extension Maintainers <pkg-mozext-maintainers@alioth-lists.debian.net>
Uploaders: Ximin Luo <infinity0@debian.org>
Build-Depends: debhelper (>= 11~), jq, mozilla-devscripts (>= 0.50~)
Homepage: https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/
Vcs-Git: https://salsa.debian.org/webext-team/tree-style-tab.git
Vcs-Browser: https://salsa.debian.org/webext-team/tree-style-tab

Package: webext-treestyletab
Architecture: all
Depends: ${misc:Depends}, ${webext:Depends}
Recommends: ${webext:Recommends}
Provides: ${webext:Provides}
Enhances: ${webext:Enhances}
Replaces: ${webext:Replaces}, xul-ext-treestyletab (<< 2)
Breaks: ${webext:Breaks}, xul-ext-treestyletab (<< 2)
Conflicts: ${webext:Conflicts}
Description: Show browser tabs like a tree
 Tree-style-tab is a Firefox/Iceweasel extension which provides tree-style tab
 bar, like a folder tree of Windows Explorer. New tabs opened from links (or
 etc.) are automatically attached to the current tab. If you often use many
 tabs, it will help your web browsing because you can understand relations of
 tabs.

# https://wiki.debian.org/RenamingPackages
Package: xul-ext-treestyletab
Depends: webext-treestyletab, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: Show browser tabs like a tree - transitional package
 This is a transitional package. It can safely be removed.
